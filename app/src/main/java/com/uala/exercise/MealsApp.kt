package com.uala.exercise

import com.uala.exercise.injection.DaggerApplicationComponent
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication

class MealsApp: DaggerApplication() {

    private val applicationInjector = DaggerApplicationComponent.builder().application(this).build()
    override fun applicationInjector(): AndroidInjector<out DaggerApplication> = applicationInjector

}