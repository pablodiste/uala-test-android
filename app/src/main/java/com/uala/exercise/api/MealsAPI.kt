package com.uala.exercise.api

import com.uala.exercise.data.Meals
import retrofit2.http.GET
import retrofit2.http.Query
import java.math.BigDecimal

interface MealsAPI {

    @GET("search.php/")
    suspend fun search(@Query("s") term: String): Meals

}