package com.uala.exercise.api

import dagger.Module
import dagger.Provides
import okhttp3.HttpUrl
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

@Module
class NetworkModule {

    // @Provides tell Dagger how to create instances of the type that this function
    @Provides
    fun provideMercadoPagoAPI(): MealsAPI {

        val httpClient = OkHttpClient.Builder()

        return Retrofit.Builder()
            .baseUrl("https://www.themealdb.com/api/json/v1/1/")
            .addConverterFactory(GsonConverterFactory.create())
            .client(httpClient.build())
            .build()
            .create(MealsAPI::class.java)
    }
}
