package com.uala.exercise.data

import com.uala.exercise.api.MealsAPI
import com.uala.exercise.util.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class MealsRepository @Inject constructor(private val mealsAPI: MealsAPI) {

    suspend fun search(term: String): Resource<List<Meal>> {
        return withContext(Dispatchers.IO) {
            try {
                val meals = mealsAPI.search(term).meals
                return@withContext Resource.success(meals)
            } catch (e: Exception) {
                return@withContext Resource.error(null, e.localizedMessage)
            }
        }
    }

}