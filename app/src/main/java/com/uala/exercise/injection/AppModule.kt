package com.uala.exercise.injection

import dagger.Module

@Module(includes = [
    ViewModelModule::class
])
class AppModule
