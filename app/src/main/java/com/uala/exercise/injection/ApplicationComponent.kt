package com.uala.exercise.injection

import android.app.Application
import com.uala.exercise.MainActivity
import com.uala.exercise.MealsApp
import com.uala.exercise.api.NetworkModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import javax.inject.Singleton

@Singleton
@Component(modules = [
    AndroidInjectionModule::class,
    AppModule::class,
    MainActivityModule::class,
    NetworkModule::class])
interface ApplicationComponent : AndroidInjector<MealsApp> {
    override fun inject(app: MealsApp)
    fun inject(activity: MainActivity)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): ApplicationComponent
    }
}