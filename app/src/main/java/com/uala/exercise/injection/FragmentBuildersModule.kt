package com.uala.exercise.injection

import com.uala.exercise.ui.mealdetails.MealDetailsFragment
import com.uala.exercise.ui.searchmeals.SearchMealsFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class FragmentBuildersModule {
    @ContributesAndroidInjector
    abstract fun contributeMealsFragment(): SearchMealsFragment
    @ContributesAndroidInjector
    abstract fun contributeMealDetailsFragment(): MealDetailsFragment
}
