package com.uala.exercise.ui.mealdetails

import android.os.Bundle
import android.view.*
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import com.uala.exercise.MainActivity
import com.uala.exercise.R
import com.uala.exercise.databinding.MealDetailsFragmentBinding
import com.uala.exercise.databinding.SearchMealsFragmentBinding
import com.uala.exercise.viewmodels.MealsViewModel
import dagger.android.support.DaggerFragment
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject
import kotlin.coroutines.CoroutineContext

class MealDetailsFragment : DaggerFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val mealsViewModel: MealsViewModel by viewModels({ activity as MainActivity }) { viewModelFactory }

    private lateinit var viewDataBinding: MealDetailsFragmentBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        viewDataBinding = MealDetailsFragmentBinding.inflate(inflater, container, false).apply {
            viewmodel = mealsViewModel
        }
        return viewDataBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewDataBinding.lifecycleOwner = this.viewLifecycleOwner
    }

}
