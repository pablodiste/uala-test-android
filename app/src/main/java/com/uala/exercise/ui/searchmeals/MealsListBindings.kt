package com.uala.exercise.ui.searchmeals

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.uala.exercise.data.Meal
import com.uala.exercise.util.Resource
import com.uala.exercise.util.Status

@BindingAdapter("items")
fun setItems(listView: RecyclerView, resource: Resource<List<Meal>>?) {
    resource?.let {
        if (resource.status == Status.SUCCESS) {
            (listView.adapter as MealsAdapter).submitList(it.data)
        }
    }
}
