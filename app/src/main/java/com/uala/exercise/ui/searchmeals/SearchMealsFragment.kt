package com.uala.exercise.ui.searchmeals

import android.os.Bundle
import android.view.*
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.uala.exercise.MainActivity
import com.uala.exercise.R
import com.uala.exercise.databinding.SearchMealsFragmentBinding
import com.uala.exercise.util.EventObserver
import com.uala.exercise.viewmodels.MealsViewModel
import dagger.android.support.DaggerFragment
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

class SearchMealsFragment : DaggerFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val mealsViewModel: MealsViewModel by viewModels({ activity as MainActivity }) { viewModelFactory }

    private lateinit var viewDataBinding: SearchMealsFragmentBinding

    private lateinit var listAdapter: MealsAdapter

    private val coroutineScope: CoroutineScope = CoroutineScope(Dispatchers.Main)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        viewDataBinding = SearchMealsFragmentBinding.inflate(inflater, container, false).apply {
            viewmodel = mealsViewModel
        }
        return viewDataBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewDataBinding.lifecycleOwner = this.viewLifecycleOwner

        setupListAdapter()
        setupNavigation()
        mealsViewModel.startSearchMeals()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.search_menu, menu)
        val myActionMenuItem = menu.findItem(R.id.action_search)
        val searchView = myActionMenuItem.actionView as SearchView
        searchView.setOnQueryTextListener (object : SearchView.OnQueryTextListener {

            private var searchFor = ""

            override fun onQueryTextChange(newText: String): Boolean {
                val searchText = newText.trim()
                if (searchText == searchFor) return false
                searchFor = searchText

                coroutineScope.launch {
                    delay(300)  //debounce timeOut
                    if (searchText != searchFor)
                        return@launch

                    mealsViewModel.searchMeals(searchText)
                }
                return false
            }

            override fun onQueryTextSubmit(query: String): Boolean {
                return false
            }
        })
    }

    private fun setupListAdapter
                () {
        val viewModel = viewDataBinding.viewmodel
        if (viewModel != null) {
            listAdapter = MealsAdapter(viewModel)
            viewDataBinding.mealsList.adapter = listAdapter
        } else {
            Timber.w("ViewModel not initialized when attempting to set up adapter.")
        }
    }

    private fun setupNavigation() {
        viewDataBinding.viewmodel?.let { viewModel ->
            viewModel.mealSelectedEvent.observe(viewLifecycleOwner, EventObserver {
                navigateToMealDetails()
            })
        }
    }

    private fun navigateToMealDetails() {
        findNavController().navigate(SearchMealsFragmentDirections.actionAmountSelectionFragmentToPaymentSelectionFragment())
    }
}
