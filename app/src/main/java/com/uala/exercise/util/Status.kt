package com.uala.exercise.util

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}