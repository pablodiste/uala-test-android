package com.uala.exercise.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.uala.exercise.data.Meal
import com.uala.exercise.data.MealsRepository
import com.uala.exercise.util.Event
import com.uala.exercise.util.Resource
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Shared ViewModel android pattern
 */
class MealsViewModel @Inject constructor(private val mealsRepository: MealsRepository): ViewModel() {

    private var _meals = MutableLiveData<Resource<List<Meal>>>()
    val meals = _meals

    private val _currentMeal = MutableLiveData<Meal>()
    val currentMeal: LiveData<Meal> = _currentMeal

    private val _mealSelectedEvent = MutableLiveData<Event<Unit>>()
    val mealSelectedEvent: LiveData<Event<Unit>> = _mealSelectedEvent

    fun startSearchMeals() {
        viewModelScope.launch {
            _meals.value = Resource.loading(null)
            val returnedMeals = mealsRepository.search("")
            _meals.value = returnedMeals
        }
    }

    fun searchMeals(term: String) {
        viewModelScope.launch {
            _meals.value = Resource.loading(null)
            val returnedMeals = mealsRepository.search(term)
            _meals.value = returnedMeals
        }
    }

    fun selectMeal(meal: Meal) {
        _currentMeal.value = meal
        _mealSelectedEvent.value = Event(Unit)
    }

}